<?php
ob_start();
/*if( !defined( 'DVWA_WEB_PAGE_TO_ROOT' ) ) {
   // echo 111;die;
	define( 'DVWA System error- WEB_PAGE_TO_ROOT undefined' );
	exit;

}*/
/*if( !defined('CAT_WEB_ROOT') ) {
	define(' error-System undefined' );
	exit;
}*/

session_start(); // Creates a 'Full Path Disclosure' vuln.
//var_dump(CAT_WEB_ROOT);die;
// Include configs
//require_once DVWA_WEB_PAGE_TO_ROOT.'config/config.inc.php';

require_once CAT_WEB_ROOT.'/config/config.inc.php';
require_once( 'dvwaPhpIds.inc.php' );

// Declare the $html variable
if(!isset($html)){

	$html = "";

}

// Valid security levels
//$security_levels = array('low', 'medium', 'high');
$security_levels = array('low', 'medium');

if (!isset($_COOKIE['security']) || !in_array($_COOKIE['security'], $security_levels))
{
    // Set security cookie to high if no cookie exists
    if (in_array($_DVWA['default_security_level'], $security_levels))
    {
        setcookie( 'security', $_DVWA['default_security_level'] );
    } 
    else setcookie('security', 'low');
}

// DVWA version
function dvwaVersionGet() {

	return '1.0';

}

// DVWA release date
function dvwaReleaseDateGet() {

	return '11/01/2011';

}


// Start session functions -- 

function &dvwaSessionGrab() {

	if( !isset( $_SESSION[ 'dvwa' ] ) ) {

		$_SESSION[ 'dvwa' ] = array();

	}
     //var_dump($_SESSION[ 'dvwa' ]) ;die;
	return $_SESSION[ 'dvwa' ];
}


function dvwaPageStartup( $pActions ) {
	
	//var_dump($pActions);die;

	if( in_array( 'authenticated', $pActions ) ) {

		if( !dvwaIsLoggedIn()){
		//	$host=$_SERVER['HTTP_HOST'].'/webTest';
			//echo $host;die;
			///echo CAT_WEB_ROOT;die;
			$host="/";
			dvwaRedirect( $host.'/login.php' );
			//dvwaRedirect( DVWA_WEB_PAGE_TO_ROOT.'login.php' );
			//dvwaRedirect( DVWA_WEB_PAGE_TO_ROOT.'index.html' );

		}
	}

	if( in_array( 'phpids', $pActions ) ) {

		if( dvwaPhpIdsIsEnabled() ) {

			dvwaPhpIdsTrap();

		}
	}
}


function dvwaPhpIdsEnabledSet( $pEnabled ) {

	$dvwaSession =& dvwaSessionGrab();

	if( $pEnabled ) {

		$dvwaSession[ 'php_ids' ] = 'enabled';

	} else {

		unset( $dvwaSession[ 'php_ids' ] );

	}
}


function dvwaPhpIdsIsEnabled() {

	$dvwaSession =& dvwaSessionGrab();

	return isset( $dvwaSession[ 'php_ids' ] );

}


function dvwaLogin( $pUsername ) {
	//echo 111;die;
    
	$dvwaSession =& dvwaSessionGrab();

	$dvwaSession['username'] = $pUsername;
	$_SESSION['username']=$pUsername;
	
	/*
	var_dump($dvwaSession['username']);
	var_dump($_SESSION['username']);
	die;
	*/

}


function dvwaIsLoggedIn() {

	$dvwaSession =& dvwaSessionGrab();
	//var_dump($_SESSION['username']);die;

	return isset( $dvwaSession['username'] );

}


function dvwaLogout() {

	$dvwaSession =& dvwaSessionGrab();

	unset( $dvwaSession['username'] );

}


function dvwaPageReload() {

	dvwaRedirect( $_SERVER[ 'PHP_SELF' ] );

}

function dvwaCurrentUser() {

	$dvwaSession =& dvwaSessionGrab();

	return ( isset( $dvwaSession['username']) ? $dvwaSession['username'] : '') ;

}

// -- END

function &dvwaPageNewGrab() {

	$returnArray = array(
		'title' => 'CAT Team (webTest) v'.dvwaVersionGet().'',
		'title_separator' => ' :: ',
		'body' => '',
		'page_id' => '',
		'help_button' => '',
		'source_button' => '',
	);

	return $returnArray;
}


function dvwaSecurityLevelGet() {

	return isset( $_COOKIE[ 'security' ] ) ? $_COOKIE[ 'security' ] : 'low';

}



function dvwaSecurityLevelSet( $pSecurityLevel ) {

	setcookie( 'security', $pSecurityLevel );

}



// Start message functions -- 
function dvwaMessagePush( $pMessage ) {

	$dvwaSession =& dvwaSessionGrab();

	if( !isset( $dvwaSession[ 'messages' ] ) ) {

		$dvwaSession[ 'messages' ] = array();

	}

	$dvwaSession[ 'messages' ][] = $pMessage;
}



function dvwaMessagePop() {

	$dvwaSession =& dvwaSessionGrab();

	if( !isset( $dvwaSession[ 'messages' ] ) || count( $dvwaSession[ 'messages' ] ) == 0 ) {

		return false;

	}

	return array_shift( $dvwaSession[ 'messages' ] );
}


function messagesPopAllToHtml() {

	$messagesHtml = '';

	while( $message = dvwaMessagePop() ) {	// TODO- sharpen!

		$messagesHtml .= "<div class=\"message\">{$message}</div>";

	}

	return $messagesHtml;
}
// --END

function domCatEcho( $pPage ) {
	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past

	echo "
	<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

	<html xmlns=\"http://www.w3.org/1999/xhtml\">

	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />

	<title>{$pPage['title']}</title>

	<link rel=\"stylesheet\" type=\"text/css\" href=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/css/main.css\" />

		<!--<link rel=\"icon\" type=\"\image/ico\" href=\"".DVWA_WEB_PAGE_TO_ROOT."favicon.ico\" />-->

		<script type=\"text/javascript\" src=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/js/dvwaPage.js\"></script>

	<script>
		function catDomXssTest()
			{
	 			var str = document.getElementById(\"input\").value;
 				document.getElementById(\"output\").innerHTML =str;
			}
	</script>

	</head>

	<body class=\"home\">
		<div id=\"container\">
			
				<div id=\"main_body\">

				{$pPage['body']}
						<br />
						<br />
						</div>
						
						<div id=\"system_info\">
						<a href=\"/logout.php\">退出登录</a>
						</div>

						<div id=\"footer\">

						<p>CAT Team (webTest)  v".dvwaVersionGet()."</p>

			</div>

		</div>

	</body>

						</html>";
}          //end


function dvwaHtmlEcho( $pPage ) {
	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past

	echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta charset=utf-8 />
<title>CAT Team攻防演练系统</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\" />
<script type=\"text/javascript\" src=\"/js/form.js\"></script>
</head>
	<body>
	{$pPage['body']}			
	</body>
</html>";
}          //end



function catHtmlEcho( $pPage ) {
$menuBlocks = array();
	//$menuBlocks['home'] = array();
	//$menuBlocks['home'][] = array( 'id' => 'home', 'name' => '首页', 'url' => '.' );
	//$menuBlocks['home'][] = array( 'id' => 'instructions', 'name' => 'Instructions', 'url' => 'instructions.php' );
	//$menuBlocks['home'][] = array( 'id' => 'setup', 'name' => 'Setup', 'url' => 'setup.php' );
	/*
	$menuBlocks['vulnerabilities'] = array();
	$menuBlocks['vulnerabilities'][] = array( 'id' => 'sqli', 'name' => 'SQL注入漏洞--基础', 'url' => './sqli/.' );
	$menuBlocks['vulnerabilities'][] = array( 'id' => 'sqli_blind', 'name' => 'SQL注入漏洞--盲注', 'url' => './sqli_blind/.' );
	$menuBlocks['vulnerabilities'][] = array( 'id' => 'sqli_blind', 'name' => 'SQL注入漏洞--宽字节', 'url' => './sqli_blind/.' );
	*/
	
	//$menuBlocks['vulnerabilities'] = array();
	$menuBlocks['Sql'][] = array( 'id' => 'sqli_base', 'name' => '基础练习', 'image'=>'/images/type1.png', 'url' => './Sql/sqli_base/index.php' );
	$menuBlocks['Sql'][] = array( 'id' => 'sqli_blind', 'name' => '盲注练习', 'image'=>'/images/type2.png', 'url' => './Sql/sqli_blind/index.php' );
	$menuBlocks['Sql'][]= array( 'id' => 'sqli_wide', 'name' => '宽字节练习', 'image'=>'/images/type3.png', 'url' => './Sql/sqli_wide/index.php' );
	
	$menuBlocks['Xss'][] = array( 'id' => 'xss_r', 'name' => '反射型xss', 'image'=>'/images/type4.png',  'url' => './Xss/xss_r/index.php' );
	$menuBlocks['Xss'][] = array( 'id' => 'xss_s', 'name' => '存储型xss',  'image'=>'/images/type5.png', 'url' => './Xss/xss_s/index.php' );
	$menuBlocks['Xss'][]= array( 'id' => 'xss_d', 'name' => 'dom型xss',  'image'=>'/images/type6.png', 'url' => './Xss/xss_d/index.php' );
	$menuBlocks['Xss'][]= array( 'id' => 'xss_m', 'name' => '突变型xss',  'image'=>'/images/type7.png', 'url' => './Xss/xss_m/index.php' );
	
	$menuBlocks['Inject'][] = array( 'id' => 'exec', 'name' => '命令注入',  'image'=>'/images/type23.png', 'url' => './Inject/cmd_inje/index.php' );
	$menuBlocks['Inject'][] = array( 'id' => 'exec', 'name' => 'XXE注入',  'image'=>'/images/type22.png', 'url' => './Inject/xml_inje/index.php' );
    
	$menuBlocks['Cam_request'][] = array( 'id' => 'csrf', 'name' => 'CSRF',  'image'=>'/images/type13.png', 'url' => './Cam_request/cam_csrf/index.php' );
	$menuBlocks['Cam_request'][] = array( 'id' => 'urlredir', 'name' => 'url重定向', 'image'=>'/images/type14.png',  'url' => './Cam_request/cam_urlredir/index.php' );
	
	$menuBlocks['File_contains'][] = array( 'id' => 'f_r', 'name' => '远程包含',  'image'=>'/images/type15.png', 'url' => './File_contains/fi_include_r/index.php?page=include.php' );
	$menuBlocks['File_contains'][] = array( 'id' => 'f_l', 'name' => '本地包含',  'image'=>'/images/type16.png', 'url' => './File_contains/fi_include_l/index.php?page=include.php' );
	
	$menuBlocks['File_operation'][] = array( 'id' => 'filedo', 'name' => '文件下载',  'image'=>'/images/type19.png', 'url' => './File_operation/filedown/index.php?filename=1.jpg' );
	$menuBlocks['File_operation'][] = array( 'id' => 'fielu', 'name' => '文件上传',  'image'=>'/images/type18.png', 'url' => './File_operation/fileupload/index.php' );
	$menuBlocks['File_operation'][] = array( 'id' => 'filede', 'name' => '文件删除', 'image'=>'/images/type17.png',  'url' => './File_operation/filedelete/index.php' );
	
	$menuBlocks['Ultra_vires'][] = array( 'id' => 'U_L', 'name' => '水平越权',  'image'=>'/images/type21.png', 'url' => './Ultra_vires/Ultra_l/index.php' );
	$menuBlocks['Ultra_vires'][] = array( 'id' => 'U_V', 'name' => '垂直越权',  'image'=>'/images/type20.png', 'url' => './Ultra_vires/Ultra_v/index.php' );
	
	$menuBlocks['Cipher_algorithm'][] = array( 'id' => 'P_R', 'name' => '弱加密',  'image'=>'/images/type8.png', 'url' => './Cipher_algorithm/Password_R/index.php' );
	//$menuBlocks['Cipher_algorithm'][] = array( 'id' => 'P_W', 'name' => '伪随机密码算法',  'image'=>'/webTest/images/type9.png', 'url' => './Cipher_algorithm/Password_W/index.php' );
	

	//$menuBlocks['Other'][] = array( 'id' => 'brute', 'name' => '暴力破解',  'image'=>'/webTest/images/type1.png', 'url' => './Other/Other_brute/index.php' );
	//$menuBlocks['Other'][] = array( 'id' => 'captcha', 'name' => 'Insecure CAPTCHA', 'image'=>'/webTest/images/type1.png',  'url' => './Other/Other_captcha/index.php' );
	//$menuBlocks['Other'][] = array( 'id' => 'xxe', 'name' => 'xxe漏洞', 'url' => './Other/Other_xxe/.' );
	$menuBlocks['Other'][] = array( 'id' => 'error', 'name' => '错误处理',  'image'=>'/images/type11.png', 'url' => './Other/Other_error/index.php' );
	$menuBlocks['Other'][] = array( 'id' => 'v_cover', 'name' => '变量覆盖', 'image'=>'/images/type12.png',  'url' => './Other/Other_cover/index.php' );


/*
	$menuBlocks['meta'] = array();
	$menuBlocks['meta'][] = array( 'id' => 'security', 'name' => 'DVWA Security', 'url' => 'security.php' );
	$menuBlocks['meta'][] = array( 'id' => 'phpinfo', 'name' => 'PHP Info', 'url' => 'phpinfo.php' );
	$menuBlocks['meta'][] = array( 'id' => 'about', 'name' => 'About', 'url' => 'about.php' );

	$menuBlocks['logout'] = array();
	$menuBlocks['logout'][] = array( 'id' => 'logout', 'name' => 'Logout', 'url' => 'logout.php' );

*/
	
	//var_dump($menuBlocks['vulnerabilities'][0]);die;

	$menuHtml = '';
	//echo "<pre>";print_r($menuBlocks['sql']);echo "<pre>";//die;
	foreach( $menuBlocks as $menuBlock ) {

	//	echo "<pre>";print_r($menuBlock);echo "<pre>";die;
	
		//echo count($menuBlock);die;
		$menuBlockHtml = '';

		foreach( $menuBlock as $menuItem ) {
			
			//echo "<pre>";print_r($menuBlock);echo "<pre>";
              // echo "<pre>";print_r($menuItem);echo "<pre>";
			//$selectedClass = ( $menuItem[ 'id' ] == $pPage[ 'page_id' ] ) ? 'selected' : '';

			$fixedUrl = DVWA_WEB_PAGE_TO_ROOT.$menuItem['url'];

			$menuBlockHtml .= "<li onclick=\"window.location='{$fixedUrl}'\" >
			<a href=\"{$fixedUrl}\" class=\"aLink\">
			<span class=\"style\"><img src=\"{$menuItem['image']}\" /></span>
			<span class=\"font\">{$menuItem['name']}</span>		
			</a>
			</li>";
			//var_dump($menuBlockHtml);
		//	$sql_injec.= "<ul>{$menuBlockHtml}</ul>";

		}    
		//var_dump((bool)strstr($menuBlockHtml,"xss"));
		if((bool)strstr($menuBlockHtml,"sql")==true){
			$sql_injec.= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
			//var_dump($sql_injec);
		}else if((bool)strstr($menuBlockHtml,"xss")==true){
			$xss .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
			//var_dump($xss);
		}else if((bool)strstr($menuBlockHtml,"inje")==true){
			$inject .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"cam")==true){
			$Cam_request .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"include")==true){
			$File_contains .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"file")==true){
			$File_operation .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"Ultra")==true){
			$Ultra_vires .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"Password")==true){
			$Cipher_algorithm .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else if((bool)strstr($menuBlockHtml,"Other")==true){
			$Other .= "<ul>{$menuBlockHtml}</ul>";
			$menuBlockHtml="";
		}else{
			echo '此漏洞不存在';
			exit;
		}
	
	}
      

	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past
	echo "
	<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
	<html xmlns=\"http://www.w3.org/1999/xhtml\">
	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
	<title>信息安全</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\" />
	<script type=\"text/javascript\" src=\"/js/jquery-1.7.2.js\"></script>
	</head>
	
	{$pPage['body']}
     
	<article>
	<ul class=\"typeBig\">
	 <li>
	 <span class=\"icon\"><img src=\"/images/t-icon1.png\" /></span>
      <span class=\"font\">SQL注入漏洞</span>
	 </li>	
	
	 <li>
	 <span class=\"icon\"><img src=\"/images/t-icon2.png\" /></span>
      <span class=\"font\">跨站脚本攻击漏洞</span>
	 </li>	
	 <li>
        	<span><img src=\"/images/t-icon3.png\" /></span>
            <span class=\"font\">注入漏洞</span>
        </li>
    	<li>
        	<span><img src=\"/images/t-icon4.png\" /></span>
            <span class=\"font1\">伪造请求漏洞</span>
        </li>
    	<li>
        	<span><img src=\"/images/t-icon5.png\" /></span>
            <span class=\"font1\">文件包含漏洞</span>
        </li>
    	<li>
        	<span><img src=\"/images/t-icon6.png\" /></span>
            <span class=\"font1\">文件操作漏洞</span>
        </li>
    	<li>
        	<span><img src=\"/images/t-icon7.png\" /></span>
            <span class=\"font\">越权漏洞</span>
        </li>
    	<li>
        	<span><img src=\"/images/t-icon8.png\" /></span>
            <span class=\"font1\">密码算法漏洞</span>
        </li>
    	<li style=\"margin-right:0px;\">
        	<span><img src=\"/images/t-icon9.png\" /></span>
            <span class=\"font\">其他WEB漏洞</span>
        </li>
				<!--  
				    <li>跨站脚本攻击漏洞{$xss}</li>
				    <li>注入漏洞</li>
				    <li>伪造请求漏洞</li>
				    <li>文件包含漏洞</li>
				    <li>文件操作漏洞</li>
				    <li>越权漏洞</li>
				    <li>密码算法漏洞</li>
				    <li>其他web漏洞</li>
				    -->
				     <br class=\"clear\"/>
				</ul>
				<br class=\"clear\"/>
<div class=\"typeSm\">{$sql_injec}</div>	
<div class=\"typeSm\" style=\"display:none;\">{$xss}</div>
<div class=\"typeSm\" style=\"display:none;\">{$inject}</div>
<div class=\"typeSm\" style=\"display:none;\">{$Cam_request}</div>
<div class=\"typeSm\" style=\"display:none;\">{$File_contains}</div>
<div class=\"typeSm\" style=\"display:none;\">{$File_operation}</div>
<div class=\"typeSm\" style=\"display:none;\">{$Ultra_vires}</div>
<div class=\"typeSm\" style=\"display:none;\">{$Cipher_algorithm}</div>
<div class=\"typeSm\" style=\"display:none;\">{$Other}</div>
				
				
				
	<img src=\"\images/logo1.png\" width=\"363\" height=\"201\" class=\"imgPos\" />		
	</article>			
<footer>CAT Team (webTest) v1.0</footer>
<script>
$(function(){
	$('.typeBig li').hover(function(){
		var i = $(this).index();
		$(this).find('img').addClass('on')
		$(this).siblings().find('img').removeClass('on');
		$(\".typeSm\").eq(i).show().siblings('.typeSm').hide();	
	},function(){});
})
</script>			  
</body>
</html>
	";
}   //end
  


function catHtmlEcho_gbk( $pPage ) {
	// Send Headers + main HTML code
	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past

	echo "
	<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

	<html xmlns=\"http://www.w3.org/1999/xhtml\">

	<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gbk\" />
    <title>信息安全</title>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\" />
	</head>
	<body class=\"home\">	
				{$pPage['body']}			
	</body>
</html>";
}

function dvwaHelpHtmlEcho( $pPage ) {
	// Send Headers
	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past

	echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

<html xmlns=\"http://www.w3.org/1999/xhtml\">

	<head>

		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />

		<title>{$pPage['title']}</title>

		<link rel=\"stylesheet\" type=\"text/css\" href=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/css/help.css\" />

		<link rel=\"icon\" type=\"\image/ico\" href=\"".DVWA_WEB_PAGE_TO_ROOT."favicon.ico\" />

	</head>

	<body>
	
	<div id=\"container\">

			{$pPage['body']}

		</div>

	</body>

</html>";
}


function dvwaSourceHtmlEcho( $pPage ) {
	// Send Headers
	Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
	Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
	Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past

	echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

<html xmlns=\"http://www.w3.org/1999/xhtml\">

	<head>

		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />

		<title>{$pPage['title']}</title>

		<link rel=\"stylesheet\" type=\"text/css\" href=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/css/source.css\" />

		<link rel=\"icon\" type=\"\image/ico\" href=\"".DVWA_WEB_PAGE_TO_ROOT."favicon.ico\" />

	</head>

	<body>

		<div id=\"container\">

			{$pPage['body']}

		</div>

	</body>

</html>";
}

// To be used on all external links --
function dvwaExternalLinkUrlGet( $pLink,$text=null ) {

	if (is_null($text)){

		return '<a href="http://hiderefer.com/?'.$pLink.'" target="_blank">'.$pLink.'</a>';

	}

	else {

		return '<a href="http://hiderefer.com/?'.$pLink.'" target="_blank">'.$text.'</a>';

	}
}
// -- END

function dvwaButtonHelpHtmlGet( $pId ) {

	$security = dvwaSecurityLevelGet();

	return "<input type=\"button\" value=\"View Help\" class=\"popup_button\" onClick=\"javascript:popUp( '".DVWA_WEB_PAGE_TO_ROOT."vulnerabilities/view_help.php?id={$pId}&security={$security}' )\">";

}


function dvwaButtonSourceHtmlGet( $pId ) {

	$security = dvwaSecurityLevelGet();

	return "<input type=\"button\" value=\"View Source\" class=\"popup_button\" onClick=\"javascript:popUp( '".DVWA_WEB_PAGE_TO_ROOT."vulnerabilities/view_source.php?id={$pId}&security={$security}' )\">";

}

// Database Management --

if ($DBMS == 'MySQL') {
 $DBMS = htmlspecialchars(strip_tags($DBMS));

 $DBMS_errorFunc = 'mysql_error()';

}
elseif ($DBMS == 'PGSQL') {

 $DBMS = htmlspecialchars(strip_tags($DBMS));

 $DBMS_errorFunc = 'pg_last_error()';

}
else {

 $DBMS = "No DBMS selected.";

 $DBMS_errorFunc = '';

}

$DBMS_connError = '<div align="center">
		<img src="'.DVWA_WEB_PAGE_TO_ROOT.'dvwa/images/logo.png">
		<pre>Unable to connect to the database.<br>'.$DBMS_errorFunc.'<br /><br /></pre>
		Click <a href="'.DVWA_WEB_PAGE_TO_ROOT.'setup.php">here</a> to setup the database.
		</div>';

function dvwaDatabaseConnect() {

	global $_DVWA;
	global $DBMS;
	global $DBMS_connError;
	
	
  // echo $DBMS;die;
	if ($DBMS == 'MySQL') {
	
		/*var_dump($_DVWA[ 'db_server' ]);
		
		var_dump($_DVWA[ 'db_user' ]);
		
        var_dump($_DVWA[ 'db_password' ]);
        die;
        */
		if( !@mysql_connect( $_DVWA[ 'db_server' ], $_DVWA[ 'db_user' ], $_DVWA[ 'db_password' ] )
		|| !@mysql_select_db( $_DVWA[ 'db_database' ] ) ) {
			die( $DBMS_connError );
		}
       
	
	}
	
	elseif ($DBMS == 'PGSQL') {

		$dbconn = pg_connect("host=".$_DVWA[ 'db_server' ]." dbname=".$_DVWA[ 'db_database' ]." user=".$_DVWA[ 'db_user' ]." password=".$_DVWA[ 'db_password' ])
		or die( $DBMS_connError );

	}
}

// -- END


function dvwaRedirect( $pLocation ) {
     //echo $pLocation;die;
	session_commit();
	header( "Location: {$pLocation}" );
	exit;

}

// XSS Stored guestbook function --
function dvwaGuestbook(){

	$query  = "SELECT name, comment FROM web_book";
	$result = mysql_query($query);

	$guestbook = '';
	
	while($row = mysql_fetch_row($result)){	
		
		if (dvwaSecurityLevelGet() == 'high'){

			$name    = htmlspecialchars($row[0]);
			$comment = htmlspecialchars($row[1]);
	
		}

		else {

			$name    = $row[0];
			$comment = $row[1];

		}
		
		$guestbook .= "<span>Name:<font>{$name}</font></span>" . "<span>Comment:<font>{$comment}</font></span>";
	} 
	
return $guestbook;
}
// -- END


?>
