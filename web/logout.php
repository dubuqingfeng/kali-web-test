<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

if( !dvwaIsLoggedIn() ) {	// The user shouldn't even be on this page
	//dvwaMessagePush( "You were not logged in" );
	dvwaRedirect( 'login.php' );
}

dvwaLogout();
dvwaMessagePush( "You have logged out" );
dvwaRedirect( 'login.php' );

?>

