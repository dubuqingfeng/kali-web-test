/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.1.73 : Database - webtest
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`webtest` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `webtest`;

/*Table structure for table `web_book` */

DROP TABLE IF EXISTS `web_book`;

CREATE TABLE `web_book` (
  `comment_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `comment` varchar(300) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `web_book` */

insert  into `web_book`(`comment_id`,`comment`,`name`) values (1,'This is a test comment.','test');

/*Table structure for table `web_users` */

DROP TABLE IF EXISTS `web_users`;

CREATE TABLE `web_users` (
  `user_id` int(6) NOT NULL DEFAULT '0',
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `user` varchar(15) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `avatar` varchar(70) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `cardID` varchar(40) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `web_users` */

insert  into `web_users`(`user_id`,`first_name`,`last_name`,`user`,`password`,`avatar`,`email`,`telephone`,`cardID`,`address`) values (1,'admin','admin','admin','e10adc3949ba59abbe56e057f20f883e','dvwa/hackable/users/admin.jpg','admin@qq.com','18801008515','110102195201069554','aaaaaaaaa'),(2,'Gordon','Brown','mmc','e10adc3949ba59abbe56e057f20f883e','dvwa/hackable/users/gordonb.jpg','G@qq.com','18801008517','110106195211045316','gggggg'),(3,'Hack','Me','1337','e10adc3949ba59abbe56e057f20f883e','dvwa/hackable/users/1337.jpg','H@qq.com','18801008518','110114195903141552','hhhhhhhh'),(4,'Pablo','Picasso','test','e10adc3949ba59abbe56e057f20f883e','dvwa/hackable/users/pablo.jpg','P@qq.com','188010085159','110111195101199411','ppppp'),(5,'Bob','Smith','fred','e10adc3949ba59abbe56e057f20f883e','dvwa/hackable/users/smithy.jpg','B@qq.com','188010085151','110104195711233951','bbbbbb');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
