<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: Cross Site Request Forgery (CSRF)';
$page[ 'page_id' ] = 'csrf';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
require_once CAT_WEB_ROOT."/vulnerabilities/web/Cam_request/cam_csrf/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'csrf';
$page[ 'source_button' ] = 'csrf';

$page[ 'body' ] .= "
		<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">客户端伪造请求漏洞演练(CSRF)</span>
    	<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>
		
<article>
	
    <div class=\"userForm\">
			<h3 class=\"pTips\">Change  admin password</h3>
    <form method=\"GET\" name=\"guestform\" action=\"#\">
      <ul>
          <li class=\"ptop1\">
              <label style=\"width:195px; text-align:right;\">New password：</label>
              <input class=\"u_input\" AUTOCOMPLETE=\"off\" name=\"password_new\" type=\"password\"/>
          </li>
          <li class=\"ptop2\">
              <label style=\"width:195px; text-align:right;\">Confirm new password：</label>
		<input class=\"u_input\" AUTOCOMPLETE=\"off\" name=\"password_conf\" type=\"password\"/>
          </li>
          <li class=\"ptop2\">
              <input style=\"margin-left:226px\" class=\"u_btn\" value=\"Change\" name=\"Change\" type=\"submit\"/>
          </li>
      </ul>
    </form>
    </div> 
   <br class=\"clearfix\"/>
   {$html}
</article>
<footer>CAT Team (webTest) v1.0</footer>
";

dvwaHtmlEcho( $page );

?>