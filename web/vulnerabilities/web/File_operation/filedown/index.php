<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'任意文件下载漏洞';
$page[ 'page_id' ] = 'upload';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
$page[ 'body' ] .= "
		
		<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">任意文件下载漏洞演练(默认下载1.jpg文件)</span>
		<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>
<body >
<article>
<div class=\"userFormDxss\">
	<a href=\"./down.php?filename=1.jpg\"> <p><font class=\"font-style\">点击下载文件</font></p></a>
		</div>
   <br class=\"clearfix\"/>
				</article>
				<footer>CAT Team v1.0</footer>
";	
dvwaHtmlEcho( $page );
//require_once CAT_WEB_ROOT."/vulnerabilities/web/File_operation/filedown/source/{$vulnerabilityFile}";

?>