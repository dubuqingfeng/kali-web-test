<?php
/**
 * 文件下载
*
**/
header("Content-type:text/html;charset=utf-8");
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
$filename=$_GET['filename'];
if($filename==""){
	$filename="1.jpg";
	download($filename);
}else{
	download($filename);
}
function download($file){
	$file=CAT_WEB_ROOT.'/vulnerabilities/web/File_operation/filedown/down/'.$file;
	//var_dump($file);die;
	$suffix = substr($file,strrpos($file,'.')); //获取文件后缀
	$down_name = md5(time()).$suffix; //新文件名，就是下载后的名字
	if(!file_exists($file)){
		die("您要下载的文件已不存在，可能是被删除");
	}
	$fp = fopen($file,"r");
	$file_size = filesize($file);
	//下载文件需要用到的头
	header("Content-type: application/octet-stream");
	header("Accept-Ranges: bytes");
	header("Accept-Length:".$file_size);
	header("Content-Disposition: attachment; filename=".$down_name);
	$buffer = 1024;
	$file_count = 0;
	//向浏览器返回数据
	while(!feof($fp) && $file_count < $file_size){
		$file_con = fread($fp,$buffer);
		$file_count += $buffer;
		echo $file_con;
	}
	fclose($fp);
}
?>