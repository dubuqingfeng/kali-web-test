<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: File Delete';
$page[ 'page_id' ] = 'upload';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
require_once CAT_WEB_ROOT."/vulnerabilities/web/File_operation/filedelete/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'upload';
$page[ 'source_button' ] = 'upload';

$page[ 'body' ] .= "
		
<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">文件删除漏洞演练</span>
    	<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>
</header>
<article>
<div class=\"userFormDxss\">
<div class=\"uploadBox\">
     <a href=\"./delete.php?filename=1.jpg\" class=\"u_link\">点击删除文件</a>(默认删除1.jpg文件)
   		<form enctype=\"multipart/form-data\" action=\"#\" method=\"POST\" style=\"padding-top:5px;\" />
			<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"100000\" />
			选择一个图片类型上传(请将删除的文件以原来的文件名重新上传一张):
			<br />
			<input name=\"uploaded\" class=\"uploaded\" type=\"file\" /><br />
			<br />
			<input type=\"submit\" class=\"upload_btn\" name=\"Upload\" value=\"Upload\" />
		</form>
		</div>
		
		</div>
   <br class=\"clearfix\"/>
				</article>
				{$html}
				<footer>CAT Team v1.0</footer>
";

dvwaHtmlEcho( $page );

?>