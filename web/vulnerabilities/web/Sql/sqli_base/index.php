<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
//var_dump($page);die;
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: SQL Injection[基础篇]';
$page[ 'page_id' ] = 'sqli';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
//echo $vulnerabilityFile;die;
require_once CAT_WEB_ROOT."/vulnerabilities/web/Sql/sqli_base/source/{$vulnerabilityFile}";
$magicQuotesWarningHtml = '';

$page[ 'body' ] .= "
		<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">SQL注入基础篇</span>
    	<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>

<article>
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"userList\">
      <tr>
        <th width=\"30%\">User Id</th>
       <th width=\"30%\">first_name</th>
        <th width=\"30%\">last_name </th>
      </tr>
    </table>
    {$html}
   <br class=\"clearfix\"/>
   
</article>
<footer>CAT Team v1.0</footer>
";
dvwaHtmlEcho( $page );

?>
