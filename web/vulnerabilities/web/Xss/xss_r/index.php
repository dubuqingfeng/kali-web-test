<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: Reflected Cross Site Scripting (XSS)';
$page[ 'page_id' ] = 'xss_r';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
require_once CAT_WEB_ROOT."/vulnerabilities/web/Xss/xss_r/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'xss_r';
$page[ 'source_button' ] = 'xss_r';

$page[ 'body' ] .= "
<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">反射型跨站脚本攻击演练（XSS）</span>
    	<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>
<article>
<div class=\"userFormDxss\">
     <p><font class=\"font-style\">{$html}</font></p>
		</div>
 </article>
<footer>CAT Team  v1.0</footer>
";
dvwaHtmlEcho( $page );

?>