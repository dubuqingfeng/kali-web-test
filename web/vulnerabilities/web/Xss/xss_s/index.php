<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: 存储型跨站脚本攻击(XSS)';
$page[ 'page_id' ] = 'xss_s';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
require_once CAT_WEB_ROOT."/vulnerabilities/web/Xss/xss_s/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'xss_s';
$page[ 'source_button' ] = 'xss_s';

$page[ 'body' ] .= "
<script type=\"text/javascript\">
        var xmlHttp;
        function createxmlHttpRequest()
        {
            if(window.ActiveXObject)
                xmlHttp = new ActiveXObject(\"Microsoft.XMLHTTP\");
            else if(window.XMLHttpRequest)
                xmlHttp = new XMLHttpRequest();
        }
        function createQueryString()
        {
           
            var queryString = \"token=\"+Math.round(new Date().getTime()/1000);
            return queryString;//防止乱码
        }
        function doRequestUsingPost()
        {
            createxmlHttpRequest();
            var url =\"source/low.php\";
            var queryString = createQueryString();
            xmlHttp.open(\"POST\",url);
            xmlHttp.onreadystatechange = handleStateChange;
            xmlHttp.setRequestHeader(\"Content-Type\",\"application/x-www-form-urlencoded\");
            xmlHttp.send(queryString);
        }
        function handleStateChange()
        {
            if(xmlHttp.readyState==4 && xmlHttp.status == 200)
            {
                alert(xmlHttp.responseText);
               // document.getElementById(\"userinfo\").innerHTML=xmlHttp.responseText;
            }
         
        }
    </script>
<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">存储型跨站脚本攻击演练（XSS）</span>
		<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>

<article>
	
    <div class=\"userForm\">
    <form method=\"post\" name=\"guestform\" onsubmit=\"return validate_form(this)\">
      <ul>
          <li class=\"ptop1\">
              <label>User Name*</label>
              <input class=\"u_input\" value=\"\" name=\"txtName\" type=\"text\"/>
          </li>
          <li class=\"ptop2\">
              <label>Comment*</label>
              <textarea class=\"u_content\" name=\"mtxMessage\"></textarea>
          </li>
          <li class=\"ptop2\">
              <input class=\"u_btn\" value=\"提交\" name=\"btnSign\" type=\"submit\"/>
		      <input class=\"u_btn\" value=\"清楚数据\"  type=\"button\" onClick=\"doRequestUsingPost();\"/>
          </li>
      </ul>
    </form>
    </div>
    
   
   <div class=\"userAdd\">
		".dvwaGuestbook()."
   </div>
   <br class=\"clearfix\"/>
   
</article>


<footer>CAT Team  v1.0</footer>
";
dvwaHtmlEcho( $page );
?>