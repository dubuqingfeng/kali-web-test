<?php
Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past
echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta charset=utf-8 />
<title>CAT Team攻防演练系统</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\" />
<script type=\"text/javascript\" src=\"/js/form.js\"></script>
<script>
		function catmXssTest()
		{
		var m = document.getElementById(\"input\").value;
		var x=document.getElementById(\"testa\");
		x.innerHTML=m;
		var html=x.innerHTML;
		alert(html); //弹出 <LISTING><img src=x onerror=alert(1)></LISTING>
		document.getElementById(\"testb\").innerHTML = html;
		}
</script>

</head>
<header>
	<div class=\"top\">
    	<img src=\"/images/top_logo.png\" class=\"top_logo\" />
    	<span class=\"world\">突变型跨站脚本攻击演练（mXSS）</span>
		<a href=\"/usercenter/index.php\" class=\"userCenter\" title=\"用户中心\">用户中心</a>
		<a href=\"/logout.php\" class=\"userCenter\">退出登录</a>
    </div>        
</header>
		
<body>		
		<article>
<div class=\"userFormDxss\">
<div id=\"output\"></div>			
		 <ul>
		<li class=\"liP\">
				<p class=\"pTips\">请在ie8或者更低版本下测试此漏洞，关于漏洞的原理和测试情况请参照此章节的实验报告</p>		
		</li>
          <li class=\"ptop2\">
              <label>Date:</label>
              <textarea class=\"u_content floatleft\" id=\"input\" name=\"mtxMessage\"></textarea>
			<div class=\"ptop2_ts floatleft\">
				<div id=\"testa\">mxss代码测试区</div>
				<div id=\"testb\">mxss代码测试区</div>	
			</div>
			<br class=\"clear\" />
          </li>
          <li class=\"ptop2\">
		      <input class=\"u_btn\" value=\"提交数据\"  type=\"button\" onClick=\"catmXssTest();\"/>
          </li>
      </ul>	
		
		</div>
   <br class=\"clearfix\"/>
				</article>
				<footer>CAT Team v1.0</footer>

		</body>

		</html>";
?>