<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
define('CAT_WEB_ROOT',str_replace( '\\' , '/' , realpath(dirname(__FILE__).'/../../../../')));
require_once CAT_WEB_ROOT.'/dvwa/includes/dvwaPage.inc.php';
//require_once DVWA_WEB_PAGE_TO_ROOT.'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: 突变型跨站脚本攻击(mXSS)';
$page[ 'page_id' ] = 'xss_r';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;
}
if($_GET['action']==medium){
	$vulnerabilityFile = 'medium.php';
}else{
	$vulnerabilityFile = 'low.php';
}
require_once CAT_WEB_ROOT."/vulnerabilities/web/Xss/xss_m/source/{$vulnerabilityFile}";
?>