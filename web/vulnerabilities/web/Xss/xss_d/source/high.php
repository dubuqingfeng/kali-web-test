<?php
Header( 'Cache-Control: no-cache, must-revalidate');		// HTTP/1.1
Header( 'Content-Type: text/html;charset=utf-8' );		// TODO- proper XHTML headers...
Header( "Expires: Tue, 23 Jun 2009 12:00:00 GMT");		// Date in the past
echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

<html xmlns=\"http://www.w3.org/1999/xhtml\">

<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />

<title>{$pPage['title']}</title>

<link rel=\"stylesheet\" type=\"text/css\" href=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/css/main.css\" />

		<!--<link rel=\"icon\" type=\"\image/ico\" href=\"".DVWA_WEB_PAGE_TO_ROOT."favicon.ico\" />-->

		<script type=\"text/javascript\" src=\"".DVWA_WEB_PAGE_TO_ROOT."dvwa/js/dvwaPage.js\"></script>

		<script>
		function catDomXssTest()
		{
		var str = document.getElementById(\"input\").value;
		document.getElementById(\"output\").innerHTML =encodeURIComponent(str);
		}
		</script>

</head>

<body class=\"home\">
<div id=\"container\">
	
<div class=\"body_padded\">
	<h1>Dom型跨站脚本攻击演练 (XSS)</h1>
<div id=\"output\"></div>

<input type=\"text\" id=\"input\" size=50 value=\"\" />

<input type=\"button\" value=\"提交\" onclick=\"catDomXssTest()\" />
</div>
				<br>
				<br>

		<div id=\"system_info\">
		<a href=\"/logout.php\">退出登录</a>
		</div>

		<div id=\"footer\">

		<p>CAT Team (webTest)  v".dvwaVersionGet()."</p>

		</div>

		</div>

		</body>

		</html>";

?>