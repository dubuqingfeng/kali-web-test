FROM kalilinux/kali-linux-docker

RUN echo "deb http://http.kali.org/kali sana main contrib non-free" > /etc/apt/sources.list && \
echo "deb-src http://http.kali.org/kali sana main contrib non-free" >> /etc/apt/sources.list && \
echo "deb http://security.kali.org/kali-security sana/updates main contrib non-free" >> /etc/apt/sources.list && \
echo "deb-src http://security.kali.org/kali-security sana/updates main contrib non-free" >> /etc/apt/sources.list

RUN echo "deb http://mirrors.aliyun.com/kali sana main non-free contrib" > /etc/apt/sources.list && \
echo "deb-src http://mirrors.aliyun.com/kali sana main non-free contrib" >> /etc/apt/sources.list && \
echo "deb http://mirrors.aliyun.com/kali-security sana/updates main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update && \
  apt-get -y install supervisor

ADD run.sh /run.sh
RUN chmod 755 /*.sh
ADD my.cnf /etc/mysql/conf.d/my.cnf
ADD supervisord-apache2.conf /etc/supervisor/conf.d/supervisord-apache2.conf
ADD supervisord-mysqld.conf /etc/supervisor/conf.d/supervisord-mysqld.conf

# Remove pre-installed database
RUN rm -rf /var/lib/mysql/*

# Configure /app folder with sample app
RUN mkdir -p /app && mkdir -p /var/www/html && rm -fr /var/www/html && ln -s /app /var/www/html

#Environment variables to configure php
ENV PHP_UPLOAD_MAX_FILESIZE 10M
ENV PHP_POST_MAX_SIZE 10M

# Add volumes for MySQL 
VOLUME  ["/etc/mysql", "/var/lib/mysql" ]

RUN chown -R www-data:www-data /var/www/html

ADD web/ /var/www/html
ADD info.php /var/www/html/info.php

ENV DB_SERVER localhost
ENV DB_DATABASE webtest
ENV DB_USER root

ADD create_mysql_admin_user.sh /create_mysql_admin_user.sh
ADD create_db.sh /create_db.sh
ADD 000-default.conf /etc/apache2/sites-available/000-default.conf

RUN chmod +x /*.sh

# Add MySQL utils
RUN chmod 755 /*.sh

EXPOSE 80 3306
CMD ["/run.sh"]